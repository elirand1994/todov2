import * as TYPES from './Types.js';
class Validator {
    static validate = async (parsedArgs : any) =>{
        console.log(parsedArgs)
        const action = parsedArgs.Action;
        const flags = parsedArgs.Flags;
        if (!(Object.values(TYPES.OPTIONS).includes(action))) {
            throw new Error(`The action ${action} doesn't exists`);
        }
        switch(action){
            case TYPES.OPTIONS.CREATE:
                await Validator.validateCreate(flags);
                break;
            case TYPES.OPTIONS.READ:
                await Validator.validateRead(flags);
                break;
            case TYPES.OPTIONS.DELETE:
                await Validator.validateDelete(flags);
                break;
            case TYPES.OPTIONS.DELETE:
                await Validator.validateDelete(flags);
                break;
            case TYPES.OPTIONS.ACTIVATE:
            case TYPES.OPTIONS.DEACTIVATE:
                await Validator.validateUpdate(action,flags);
                break;
        }

        }

        static validateCreate = async (flags : string[]) =>{
            if (flags.length !== 2){
                throw new Error('A task must have a title and description');
            }
        }
        static validateRead = async (flags : string[]) =>{
            if (flags.length !== 1) {
                throw new Error(`${TYPES.OPTIONS.READ} must have 1 parameter : ${TYPES.STATES.ALL},${TYPES.STATES.ACTIVE},${TYPES.STATES.COMPLETED}`);
            }
            const [state] = flags;
            if (!(Object.values<string>(TYPES.STATES).includes(state))) {
                throw new Error(`No such ${state} exists, please choose : ${TYPES.STATES.ALL},${TYPES.STATES.ACTIVE},${TYPES.STATES.COMPLETED}`)
            }
        }
        // validate again
        static validateDelete = async (flags : string[]) =>{
            if (flags.length !== 1){
                throw new Error(`${TYPES.OPTIONS.DELETE} must have 1 parameter : ${TYPES.STATES.ALL},${TYPES.STATES.ACTIVE},${TYPES.STATES.COMPLETED} or an id`)
            }
        }
        //validate again
        static validateUpdate = async (action : string,flags : string[]) =>{
            if (flags.length !== 1){
                throw new Error(`${action} must have 1 parameter : id`);
            }
        }
    }

export default Validator;