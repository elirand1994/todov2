export enum STATES {
    COMPLETED = "completed",
    ALL = "all",
    ACTIVE = "active",
}

export enum OPTIONS {
    CREATE = "create",
    READ = "read",
    UPDATE = "update",
    DELETE = "delete",
    ACTIVATE = "activate",
    DEACTIVATE = "deactivate"
}
