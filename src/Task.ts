interface Task {
    "title" : string,
    "description" : string,
    "isComplete" : boolean,
    "id" : string
}

export default Task;