class ArgsParser {
    static parse = async(processArgs : string[]) =>{
        const argv = processArgs.slice(2);
        const action = argv[0];
        const flag = [...argv.slice(1)];
        const args = {
            'Action' : action,
            'Flags' : flag
        }
        return args;
    }
}

export default ArgsParser;