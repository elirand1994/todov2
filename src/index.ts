import ArgsParser from './ArgsParser.js';
import Validator from './Validator.js';
import Controller from './Controller.js';
import {load} from './etc.js';
import log from '@ajar/marker';
const main = async () =>{
    try {
    const parsed = await ArgsParser.parse(process.argv);
    await Validator.validate(parsed);
    const todos = await load();
    const controller = new Controller(todos)
    await controller.runTask(parsed);
    } catch(err) {
        log.red(err);
    }
}


main();