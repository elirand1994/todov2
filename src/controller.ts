import log from "@ajar/marker";
import fs from 'fs/promises';
import {generateId,path} from './etc.js';
import * as TYPES from './Types.js';
import Task from './Task.js'
class Controller {
    private todos;
    constructor(todos: Task[]){
        this.todos = todos;
    }

    saveToDos = async (todos : Task[] | undefined) =>{
        await fs.writeFile(path,JSON.stringify(todos))
        
    }

    createHandler = async (args : string[]):Promise<Task[]>  => {
        log.blue("Creating the task...")
        console.log(args);
        const [title,description] = args;
        const task : Task = {
            "title" : title,
            'description' : description,
            "isComplete" : false,
            "id" : generateId()
        };
        this.todos.push(task);
        return this.todos;
    }
    
    readHandler  = async (params : string[])  =>{
        let list =[];
        const [read] = params;
            switch(read){
                case TYPES.STATES.COMPLETED:
                    list = this.todos.filter((task: { isComplete: boolean; })=> task.isComplete === true);
                    break;
                case TYPES.STATES.ACTIVE:
                    list = this.todos.filter(((task: { isComplete: boolean; }) => task.isComplete === false));
                    break;
                case TYPES.STATES.ALL:
                    list = this.todos;
                    break;
                default:
                    list = this.todos;
                    break; 
        }
        return list;
    }

    output = async (list : Task[]) => {
        log.magenta('LIST OF TASKS:')
        for (const task of list){
            log.cyan(`Task ID : ${task.id}`);
            log.magenta(`Title : ${task.title}`)
            if (task.description !== undefined) {
                log.magenta(`Description : ${task.description}`)
            }
            task.isComplete? log.green('Task has been completed!') : log.red('Task has not been commpleted yet...');
            log.yellow('------------------------------------');
        }
    }

    
    updateHandler = async (params : string, status : boolean):Promise<Task[]> =>{
        const [id] = params;
        const task = this.todos.find((element: { id: any; }) => element.id === id);
        if (task === undefined) {
            throw new Error(`Task ${id} is not found!`);
        }
        task.isComplete = status;
        return this.todos;
    }

    deleteCompleted = async ():Promise<Task[]>  => {
        const list = this.todos.filter((task: { isComplete: boolean; }) => task.isComplete === false);
        return list;
    }

    deleteActive = async ():Promise<Task[]>  => {
        const list = this.todos.filter((task: { isComplete: boolean; }) => task.isComplete === true);
        return list;
    }

    deleteAll = async ():Promise<Task[]>  => {
        this.todos = [];
        return this.todos;
    }
    
    deleteByID = async (id : string ) : Promise<Task[]> =>{
        const [taskId] = id;
        const updatedTodos = this.todos.filter((element: { id: any; }) => element.id !== taskId);
        return updatedTodos;
    }
    
    showInfo = () =>{
        const text = `
        How to use:
         npm run build -> builds the project
         npm run start create "title" -> creates a new task with the given title
         npm run start read -> retrieves all the tasks and displays in a formated version
         npm run start update "task_id" "true|false" -> updates a given task based on the id, completed or not
         npm run start delete "task_id" -> deletes the given task from the todolist
         npm run start clear -> removes all the completed tasks`;
        log.cyan(text);
    }
    
    runTask = async (args : any) =>{
        let todos;
        switch (args.Action){
            case TYPES.OPTIONS.CREATE:
                todos = await this.createHandler(args.Flags);
                break;
            case TYPES.OPTIONS.READ:
                const filtered = await this.readHandler(args.Flags);
                this.output(filtered);
                return;
            case TYPES.OPTIONS.ACTIVATE:
                todos = await this.updateHandler(args.Flags,true);
                break;
            case TYPES.OPTIONS.DEACTIVATE:
                todos = await this.updateHandler(args.Flags,false);
                break;
            case TYPES.OPTIONS.DELETE:
                if (args.Flags[0] === TYPES.STATES.COMPLETED) {
                    todos = await this.deleteCompleted();
                    break;
                }
                if (args.Flags[0] === TYPES.STATES.ACTIVE) {
                    todos = await this.deleteActive();
                    break;
                }
                if (args.Flags[0] === TYPES.STATES.ALL) {
                    todos = await this.deleteAll();
                    break;
                }
                else {todos = await this.deleteByID(args.Flags);}
                break;
            default:
                break;
            }
        this.saveToDos(todos);   
    }
    
}

export default Controller;


